from django.contrib import admin
from django.urls import path
from rest_framework import routers
from videoviewer.views import HistoryViewset, BookmarkViewSet

router = routers.SimpleRouter()
router.register(r'history', HistoryViewset, basename='History')
router.register(r'bookmarks', BookmarkViewSet, basename='Bookmarks')

urlpatterns = router.urls
