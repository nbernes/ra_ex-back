from distutils.log import error
import re
from rest_framework import viewsets, mixins
from rest_framework.response import Response
from rest_framework import status
from .models import History, Bookmark
from .serializers import BookmarkSerializer


class HistoryViewset(viewsets.GenericViewSet):

    """
        Returns a list of youtube video ids
    """
    def list(self, _):
        items = History.objects.get().video_ids
        return Response(items)

    """
        accepts a 'video_id' parameter in body containing the youtube video ID
    """
    def create(self, req):
        new_video_id: str = req.data.get('video_id')
        if not re.match(r'^\S{11}$', new_video_id):
            return Response(data=error, status=status.HTTP_400_BAD_REQUEST)
        instance = History.objects.get()
        video_ids = instance.video_ids
        video_ids.append({
            "video_id": new_video_id
        })
        instance.save()
        return Response(video_ids)



class BookmarkViewSet(
    mixins.CreateModelMixin,
    viewsets.GenericViewSet):
    
    serializer_class = BookmarkSerializer

    """
        Returns a list of Bookmarks sorted by creation date in descending order
    """
    def list(self, _):
        queryset = Bookmark.objects.all().order_by('-created')
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
