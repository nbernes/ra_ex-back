from tabnanny import verbose
from time import time_ns
from django.db import models
from django.utils import timezone

class  History(models.Model):
    items = models.JSONField()

    @property
    def video_ids(self) -> list:
        return self.items
    

class Bookmark(models.Model):
    created = models.DateTimeField()
    video_id = models.CharField(max_length=11)

    def save(self, *args, **kwargs):
        self.created = timezone.now()
        super(Bookmark, self).save(*args, **kwargs)
