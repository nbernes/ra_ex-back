# Relief Applications exercise (back-end)
Simple Django API to bookmark YouTube videos and store the user's history.

## Installation

### Local / development setup
Requires **Python3** and **pip3** to run.

To install dependencies and initialize the database, run at the root of the repository:
```sh
pip3 install -r requirements.txt
python3 manage.py migrate
```
Then run:
```sh 
python3 manage.py runserver
```
to launch the server locally on port **8000**.

### Docker setup
> :warning: **Using this setup, the database does not persist across container restarts.**

Requires that you have **Docker** installed.

At the root of the repository, run:
```sh
docker build -t api .
docker run -p 8000:8000 api
```
The server will then be available locally on port **8000**.