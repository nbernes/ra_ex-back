FROM python:latest

WORKDIR /app

# Install all packages specified in requirements.txt
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
RUN pip3 install gunicorn

# Copy source code
COPY . .

# expose api port
EXPOSE 8000

# run migrations and launch server
RUN python3 manage.py migrate
CMD gunicorn api.wsgi --bind 0.0.0.0:8000